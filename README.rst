MultiObjectRegistrationFramework
=================================

.. |CircleCI| image:: https://circleci.com/gh/InsightSoftwareConsortium/MultiObjectRegistrationFramework.svg?style=shield
    :target: https://circleci.com/gh/InsightSoftwareConsortium/MultiObjectRegistrationFramework

.. |TravisCI| image:: https://travis-ci.org/InsightSoftwareConsortium/MultiObjectRegistrationFramework.svg?branch=master
    :target: https://travis-ci.org/InsightSoftwareConsortium/MultiObjectRegistrationFramework

.. |AppVeyor| image:: https://img.shields.io/appveyor/ci/kabcode/multiobjectregistrationframework.svg
    :target: https://ci.appveyor.com/project/kabcode/multiobjectregistrationframework

=========== =========== ===========
   Linux      macOS       Windows
=========== =========== ===========
|CircleCI|  |TravisCI|  |AppVeyor|
=========== =========== ===========

Framework for registration of multiple objects

This module allows for registration of mutliple objects towards a single image/volume
