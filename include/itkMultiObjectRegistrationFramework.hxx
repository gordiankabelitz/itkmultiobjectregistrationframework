#ifndef itkMultiObjectRegistrationFramework_hxx
#define itkMultiObjectRegistrationFramework_hxx

#include "itkMultiObjectRegistrationFramework.h"

namespace itk
{
	template<typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::MultiObjectRegistrationFramework():
	Superclass()
	{
		// Empty input container
		this->m_MovingObjects.clear();
		this->m_InitialTransforms.clear();
		this->m_FixedImages.clear();
		this->m_ProjectionGeometries.clear();
		
		// Empty component container
		this->m_DRREngines.clear();
		this->m_Optimizers.clear();
		this->m_CompositeMetrics.clear();
		this->m_Transforms.clear();

		// Set component master
		this->m_OptimizerMaster = nullptr;
		this->m_TransformMaster = nullptr;
		this->m_DRREngineMaster = nullptr;
		this->m_MetricMaster = nullptr;

		// Empty intermediate variables
		this->m_CurrentMovingObjectSet   = 0;
		this->m_CurrentFixedImageSet     = 0;
		this->m_Stop = false;

		// Empty output container
		TransformOutputPointer TransformDecorator = nullptr;

	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::~MultiObjectRegistrationFramework()
	{
		/** Empty input container */
		this->m_MovingObjects.clear();
		this->m_InitialTransforms.clear();
		this->m_FixedImages.clear();
		this->m_ProjectionGeometries.clear();

		/** Empty component container */
		this->m_DRREngines.clear();
		this->m_Optimizers.clear();
		this->m_CompositeMetrics.clear();
		this->m_Transforms.clear();

		/** Empty intermediate variables */
		this->m_CurrentMovingObjectSet   = 0;
		this->m_CurrentFixedImageSet     = 0;
		this->m_Stop = false;
	}

	/******* INPUT HANDLING *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::AddMovingObject(const Moving3DImagePointer object)
	{
		for (auto i = 0; i < this->GetNumberOfObjects(); ++i)
		{
			// object is already in the container
			if (this->m_MovingObjects[i] == object)
				return;
		}
			
		this->m_MovingObjects.push_back(object);

		// Set initial transform (can be replaced by provided initial transform)
		this->m_InitialTransforms.push_back(nullptr);

		// Create registration entries for the moving object
		this->m_Transforms.push_back(nullptr);
		this->m_CompositeMetrics.push_back(CompositeMetricType::New());
		this->m_Optimizers.push_back(nullptr);

		// Create an output object
		TransformOutputPointer OutputTransform = 
			static_cast<TransformOutputType*>(
				this->MakeOutput(this->GetNumberOfObjects()).GetPointer());
		this->Superclass::SetNthOutput(this->GetNumberOfObjects(), OutputTransform);
		this->Modified();
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::Moving3DImagePointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNthMovingObject(unsigned int n)
	{
		if (n >= this->m_MovingObjects.size())
			return nullptr;
		return this->m_MovingObjects[n];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::RemoveNthMovingObject(unsigned int n)
	{
		if (n >= this->GetNumberOfObjects())
			return;
		this->m_MovingObjects[n] = nullptr;
		this->m_MovingObjects.erase(this->m_MovingObjects.begin() + n);
		this->m_InitialTransforms[n] = nullptr;
		this->m_InitialTransforms.erase(this->m_InitialTransforms.begin() + n);
		this->m_Transforms[n] = nullptr;
		this->m_Transforms.erase(this->m_Transforms.begin() + n);
		this->m_CompositeMetrics[n] = nullptr; /// needs to remove submetrics
		this->m_CompositeMetrics.erase(this->m_CompositeMetrics.begin() + n);
		this->m_Optimizers[n] = nullptr;
		this->m_Optimizers.erase(this->m_Optimizers.begin() + n);
		/** \todo Check for variables that are effected by removing a single object. */

		/** \todo What happens with The TransformOutputPointer when the nth object is removed? */
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	std::size_t
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNumberOfObjects() const
	{
		return m_MovingObjects.size();
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetNthInitialTransform(unsigned int n, const InitialTransformPointer transform)
	{
		if (n >= this->GetNumberOfObjects())
			return; // not enough moving objects
		if (this->m_MovingObjects[n] == nullptr)
			return; // the moving object is missing;
		if (this->m_InitialTransforms[n] != transform)
		{
			this->m_InitialTransforms[n] = transform;
			this->Modified();
		}
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::InitialTransformPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNthInitialTransform(unsigned int n) const 
	{
		if (n >= this->m_MovingObjects.size())
			return nullptr;
		return this->m_InitialTransforms[n];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::AddMovingObjectWithInitialTransform(const Moving3DImagePointer object, const InitialTransformPointer transform)
	{
		this->AddMovingObject(object);
		this->SetNthInitialTransform(this->GetNumberOfObjects()-1, transform);
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::RemoveAllMovingObjectsAndInitialTransforms()
	{
		for (auto i = 0; i < this->GetNumberOfObjects(); ++i)
		{
			this->m_MovingObjects[i] = nullptr;
			this->m_InitialTransforms[i] = nullptr;
			this->m_Transforms[i] = nullptr;
			this->m_CompositeMetrics[i] = nullptr;
			this->m_Optimizers[i] = nullptr;
		}
		this->m_MovingObjects.clear();
		this->m_InitialTransforms.clear();
		this->m_Transforms.clear();
		this->m_CompositeMetrics.clear();
		this->m_Optimizers.clear();

		/** \todo Check for other variables that are affected from removing all objects. */

	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::AddFixedImageAndGeometry(const Fixed2DImagePointer fixedimage, const ProjectionPointer geometry)
	{
		for (auto i = 0; i < this->GetNumberOfFixedImages(); ++i)
		{
			// image is already in the container
			if (this->m_FixedImages[i] == fixedimage)
				return;
		}

		this->m_FixedImages.push_back(fixedimage);
		this->m_ProjectionGeometries.push_back(geometry);
		this->Modified();
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::Fixed2DImagePointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetMthFixedImage(unsigned int m)
	{
		if (m >= this->m_FixedImages.size())
			return nullptr;
		return this->m_FixedImages[m];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::RemoveMthFixedImage(unsigned int m)
	{
		if (m >= this->GetNumberOfFixedImages())
			return;

		this->m_FixedImages[m] = nullptr;
		this->m_FixedImages.erase(this->m_FixedImages.begin() + m);
		this->m_ProjectionGeometries[m] = nullptr;
		this->m_ProjectionGeometries.erase(this->m_ProjectionGeometries.begin() + m);
		this->m_DRREngines[m] = nullptr;
		this->m_DRREngines.erase(this->m_DRREngines.begin() + m);
		/** \todo Check for variables that are effected by removing a fixed image. */
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	std::size_t
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNumberOfFixedImages() const
	{
		return this->m_FixedImages.size();
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetMthProjectionGeometry(unsigned int m, const ProjectionPointer geometry)
	{
		if (m >= this->GetNumberOfFixedImages())
			return; // m is too large, no corresponding images
		if (this->m_FixedImages[m] == nullptr)
			return; // image must be set before setting the geometry (this should not occur)
		if (this->m_ProjectionGeometries[m] != geometry)
		{
			this->m_ProjectionGeometries[m] = geometry;
			this->Modified();
		}
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::ProjectionPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetMthProjectionGeometry(unsigned int m)
	{
		if (m >= this->m_ProjectionGeometries.size())
			return nullptr;
		return this->m_ProjectionGeometries[m];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::RemoveAllFixedImagesAndProjectionGeometries()
	{
		for (auto i = 0; i < this->GetNumberOfFixedImages(); ++i)
		{
			this->m_FixedImages[i] = nullptr;
			this->m_ProjectionGeometries[i] = nullptr;
			this->m_DRREngines[i] = nullptr;
		}

		this->m_FixedImages.clear();
		this->m_ProjectionGeometries.clear();
		this->m_DRREngines.clear();
	}

	/******* COMPONENT HANDLING *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetDRREngineMaster(DRREnginePointer drrEngine)
	{
		if (this->m_DRREngineMaster == drrEngine)
			return;
		this->m_DRREngineMaster = drrEngine;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::DRREnginePointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetDRREngineMaster()
	{
		return this->m_DRREngineMaster;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetMthDRREngine(unsigned int m, DRREnginePointer drrEngine)
	{
		if (m >= this->m_DRREngines.size())
			return;
		this->m_DRREngines[m] = drrEngine;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::DRREnginePointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetMthDRREngine(unsigned int m)
	{
		if (m >= this->m_DRREngines.size())
			return nullptr;
		return this->m_DRREngines[m];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetMetricMaster(BaseMetricPointer metric)
	{
		if (this->m_MetricMaster == metric)
			return;
		this->m_MetricMaster = metric;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::BaseMetricPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetMetricMaster()
	{
		return this->m_MetricMaster;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::BaseMetricPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNthCompositeMetric(unsigned int n)
	{
		if (n >= this->GetNumberOfObjects())
			return nullptr;

		return this->m_CompositeMetrics[n];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetTransformMaster(TransformPointer transform)
	{
		if (this->m_TransformMaster == transform)
			return;
		this->m_TransformMaster = transform;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::TransformPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetTransformMaster()
	{
		return this->m_TransformMaster;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetNthTransform(unsigned int n, TransformPointer transform)
	{
		if (n >= this->GetNumberOfObjects())
			return;
		this->m_Transforms[n] = transform;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::TransformPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNthTransform(unsigned int n)
	{
		if (n >= this->GetNumberOfObjects())
			return nullptr;
		return this->m_Transforms[n];
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetOptimizerMaster(OptimizerPointer optimizer)
	{
		if (this->m_OptimizerMaster == optimizer)
			return;
		this->m_OptimizerMaster = optimizer;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::OptimizerPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetOptimizerMaster()
	{
		return this->m_OptimizerMaster;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::SetNthOptimizer(unsigned int n, OptimizerPointer optimizer)
	{
		if (n >= this->GetNumberOfObjects())
			return;
		this->m_Optimizers[n] = optimizer;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::OptimizerPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetNthOptimizer(unsigned int n)
	{
		if (n >= this->GetNumberOfObjects())
			return nullptr;
		return this->m_Optimizers[n];
	}


	/******* INITIALIZATION *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	bool
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::Initialize() throw (itk::ExceptionObject)
	{
		// Input checking
		if (this->GetNumberOfObjects() == 0)
			itkExceptionMacro("No objects set.");

		if (this->GetNumberOfFixedImages() == 0)
			itkExceptionMacro("No projections set.");

		if (this->m_DRREngineMaster == nullptr)
			itkExceptionMacro("No engine master set.")

		if (this->m_MetricMaster == nullptr)
			itkExceptionMacro("No mectric master set.")
		
		if (this->m_OptimizerMaster == nullptr)
			itkExceptionMacro("No optimizer master set.")
		
		if (this->m_TransformMaster == nullptr)
			itkExceptionMacro("No transform master set.")

		for (auto n = 0; n < this->GetNumberOfObjects(); ++n)
		{
			// for each object a submetric is created
			if (this->m_CompositeMetrics[n]->GetSubmetric() == nullptr)
			{
				this->m_CompositeMetrics[n]->SetSubmetricMaster(this->m_MetricMaster);
			}

			// if no initial transform is provided an identity transform is created
			if (this->m_InitialTransforms[n] == nullptr)
			{
				auto InitialTransform = MatrixOffsetTransformBase<double>::New();
				InitialTransform->SetIdentity();
				this->m_InitialTransforms[n] = InitialTransform;
			}

			// for each composite an optimizer is created
			if(this->m_Optimizers[n] == nullptr)
			{
				this->m_Optimizers[n] = this->m_OptimizerMaster->Clone();
			}
				
			// for each object a transform is created
			if (this->m_Transforms[n] == nullptr)
			{
				this->m_Transforms[n] = this->m_TransformMaster->Clone();
			}
				
			// create projectors for each fixed image and each object
			for (auto m = 0; m < this->GetNumberOfFixedImages(); ++m)
			{
				this->m_DRREngines.push_back(DRREngineType::New());
			}			
		}
		return true;
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::InitializeDRRComponents() throw(itk::ExceptionObject)
	{
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::InitializeTransformComponents() throw(itk::ExceptionObject)
	{
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::InitializeCompositeMetricComponents() throw(itk::ExceptionObject)
	{
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::InitializeOptimizerComponents() throw(itk::ExceptionObject)
	{
	}

	/******* OUTPUT HANDLING *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	const typename MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>::TransformOutputType*
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GetOutput(unsigned int n) const
	{
		if (n >= this->GetNumberOfObjects())
			return nullptr;

		return static_cast<const TransformOutputType*>(this->Superclass::GetOutput(n));
	}

	/******* PROCESSING METHODS *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	ProcessObject::DataObjectPointer
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::MakeOutput(unsigned int n)
	{
		if( n >= this->GetNumberOfObjects() )
			return nullptr;

		return static_cast<DataObject*>(TransformOutputType::New().GetPointer());
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::GenerateData() throw(itk::ExceptionObject)
	{
		try
		{
			std::cout << "GenerateData" << std::endl;
		}
		catch (itk::ExceptionObject &EO)
		{
			EO.Print(std::cout);
		}
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::AddInternalObservers()
	{
	}

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::RemoveInternalObservers()
	{
	}

	/******* INFORMATION METHODS *******/

	template <typename TFixed2DImage, typename TMoving3DImage, typename TVirtualImage>
	void
	MultiObjectRegistrationFramework<TFixed2DImage, TMoving3DImage, TVirtualImage>
	::PrintSelf(std::ostream& os, Indent indent) const
	{
	}
	
} // end namespace itk


#endif
