#ifndef itkLabelImageToMultipleImageFilter_hxx
#define itkLabelImageToMultipleImageFilter_hxx
#include "itkLabelImageToMultipleImageFilter.h"

namespace itk
{
	
	template<typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::LabelImageToMultipleImageFilter()
	{
		this->m_LabelImageToShapeLabelMapFilter = LabelImageToShapeLabelMapFilterType::New();
		this->m_RegionOfInterestFilter = RegionOfInterestImageFilterType::New();
		this->m_LabelMapPointer = nullptr;
		this->m_NumberOfLabels = 0;
		this->m_CurrentLabel = -1;
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::~LabelImageToMultipleImageFilter()
	{
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::SetInput(LabelImagePointer InputLabelImage)
	{
		if(this->GetInput() != InputLabelImage)
		{
			this->Superclass::SetInput(InputLabelImage);
			ComputeLabelMap();
		}
	}

	/******* PROCESSING METHODS *******/

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::GenerateData()
	{
		auto InputImagePointer = LabelImageType::New();
		InputImagePointer->Graft(const_cast<LabelImageType*>(this->GetInput()));

		m_RegionOfInterestFilter->SetInput(InputImagePointer);

		for (auto i = 0; i < m_NumberOfLabels; ++i)
		{
			auto LabelObject = m_LabelMapPointer->GetNthLabelObject(i);

			m_RegionOfInterestFilter->SetRegionOfInterest(LabelObject->GetBoundingBox());
			try
			{
				m_RegionOfInterestFilter->GraftOutput(this->GetOutput(i));
				m_RegionOfInterestFilter->Update();
			}
			catch (ExceptionObject &EO)
			{
				EO.Print(std::cout);
			}

			this->GetOutput(i)->Graft(m_RegionOfInterestFilter->GetOutput());
		}
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::GenerateOutputInformation()
	{
		for( auto idx = 0; idx < m_NumberOfLabels; ++idx)
		{
			OutputImagePointer Output = this->GetOutput(idx);
			auto OutputObject = m_LabelMapPointer->GetNthLabelObject(idx);
			auto OutputRegion = OutputObject->GetBoundingBox();

			Output->SetRegions(OutputRegion);
			Output->Allocate();

			auto InputImage = this->GetInput();

			auto OutputSpacing = InputImage->GetSpacing();
			Output->SetSpacing(OutputSpacing);

			auto OutputOrigin = InputImage->GetOrigin(); // This might be not correct!
			Output->SetOrigin(OutputOrigin);
		}
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::GenerateInputRequestedRegion()
	{
		this->Superclass::GenerateInputRequestedRegion();
		// get pointer to the input
		if (!this->GetInput())
			return;
		LabelImagePointer InputPtr = const_cast<LabelImageType*> (this->GetInput());

		// request complete input image
		InputPtr->SetRequestedRegionToLargestPossibleRegion();
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::EnlargeOutputRequestedRegion(itk::DataObject* data)
	{
		this->Superclass::EnlargeOutputRequestedRegion(data);
		data->SetRequestedRegionToLargestPossibleRegion();
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::GenerateOutputRequestedRegion(itk::DataObject* data)
	{
		for (auto idx = 0; idx < m_NumberOfLabels; ++idx)
		{
			OutputImagePointer Output = this->GetOutput(idx);
			if(Output)
			{
				Output->SetRequestedRegionToLargestPossibleRegion();
				Output->SetBufferedRegion(Output->GetLargestPossibleRegion());
			}
		}
	}

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::ComputeLabelMap()
	{
		if(this->GetInput())
		{
			this->m_LabelImageToShapeLabelMapFilter->SetInput(this->GetInput());
			try
			{
				this->m_LabelImageToShapeLabelMapFilter->Update();
			}
			catch (itk::ExceptionObject &EO)
			{
				EO.Print(std::cout);
			}
			
			this->m_LabelMapPointer = this->m_LabelImageToShapeLabelMapFilter->GetOutput();

			// Adjust the number of outputs to the number of labels in the input image
			this->m_NumberOfLabels = this->m_LabelMapPointer->GetNumberOfLabelObjects();
			
			for (auto i = 0; i < m_NumberOfLabels; ++i)
			{
				OutputImagePointer OutputLabelImage = static_cast<OutputImageType *>(this->MakeOutput(i).GetPointer());
				this->SetNthOutput(i, OutputLabelImage);
			}

			if (m_NumberOfLabels >= 0)
				m_CurrentLabel = 0;

			this->SetNumberOfRequiredOutputs(m_NumberOfLabels);
			this->Modified();
		}
	}

	/******* INFORMATION METHODS *******/

	template <typename TLabelImage, typename TIntensityImage, typename TOutputImage>
	void
	LabelImageToMultipleImageFilter<TLabelImage, TIntensityImage, TOutputImage>
	::PrintSelf(std::ostream& os, Indent indent) const
	{
		Superclass::PrintSelf(os, indent);
		os << indent << "Number of Labels: " << m_NumberOfLabels << std::endl;
		os << indent << "Current number of label: " << m_CurrentLabel << std::endl;
	}

}// end namespace itk

#endif