/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 #ifndef itkLabelImageToMultipleImageFilter_h
 #define itkLabelImageToMultipleImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkLabelImageToShapeLabelMapFilter.h"
#include "itkRegionOfInterestImageFilter.h"


namespace itk
{
	/** \class LabelImageToMultipleImageFilter
	* \brief Divides a segmented image into multiple images with a single object
	*
	* This filter is templated over a labeled image and an intensitx image. The label image is
	* divided into different region each one containing a single object. Then the regions are
	* put into single image outputs.
	* This filter as a single input images and produces mulitple output images. Following filters
	* need to handle the outputs. The number of expected outputs can be queried using
	* itk::LabelImageToMultipleImageFilter::GetNumberOfLabels(). The new outputs can be provided as
	* subparts of the label image or the intensity image. This class is a composite filter of
	* itk::LabelImageToShapeLabelMapFilter and itk::RegionOfInterestImageFilter. 
	*
	* \author  Gordian Kabelitz
	* \date    03/07/2018
	* \version 0.5
	* \ingroup MultiObjectRegistrationFramework
	*/
	template< typename TLabelImage, typename TIntensityImage = TLabelImage, typename TOutputImage = TLabelImage>
	class ITK_TEMPLATE_EXPORT LabelImageToMultipleImageFilter :
	public ImageToImageFilter<TIntensityImage, TOutputImage>
	{
	public:
		ITK_DISALLOW_COPY_AND_ASSIGN(LabelImageToMultipleImageFilter); ///< Prevent copying this object

		//@{
		/** Standard class type aliases */
		using Self         = LabelImageToMultipleImageFilter;
		using Superclass   = ImageToImageFilter<TLabelImage, TIntensityImage>;
		using Pointer      = SmartPointer<Self>;
		using ConstPointer = SmartPointer<const Self>;
		//@}

		//@{
		/** Standard class macro */
		itkNewMacro(Self);
		itkTypeMacro(LabelImageToMultipleImageFilter, ImageToImageFilter);
		//@}

		//@{
		/** Class definitions */
		using LabelImageType = TLabelImage;
		using LabelPixelType = typename LabelImageType::PixelType;
		using LabelImagePointer = typename LabelImageType::Pointer ;
		using IntensityImageType = TIntensityImage;
		using IntensityPixelType = typename IntensityImageType::PixelType;
		using IntensityImagePointer = typename IntensityImageType::Pointer;
		using OutputImageType = TOutputImage;
		using OutputImagePointer = typename OutputImageType::Pointer;

		using Labeltype = int;
		using ShapeLabelObjectType = ShapeLabelObject<Labeltype, LabelImageType::ImageDimension>;
		using LabelMapType = LabelMap<ShapeLabelObjectType>;
		using LabelMapTypePointer = LabelMapType*;
		//@}

		//@{
		/** Constructor and Destructor */
		LabelImageToMultipleImageFilter();
		~LabelImageToMultipleImageFilter() override;
		//@}

		/******* INPUT HANDLING *******/

		/**
		*   Set the labeled image as main input. The labeled image is the based for the output objects.
		*   Every time the input is set a new label map is computed to extract the number of labels.
		*   \param[in]  InputLabelImage Image with n different label of n objects
		*   \return void
		*/
		virtual void SetInput(LabelImagePointer InputLabelImage);

		/**
		*   Get the number of independent label objects.
		*   \return Number of independent label
		*/
		itkGetConstMacro(NumberOfLabels, int);
		/**
		*   Get the current label numbe. The label number is based on the labeled image. 
		*   \return Number of curent label
		*/
		itkGetConstMacro(CurrentLabel, int);

	protected:

		//@{
		/** Class components definitions */
		using LabelImageToShapeLabelMapFilterType    = LabelImageToShapeLabelMapFilter<LabelImageType, LabelMapType>;
		using LabelImageToShapeLabelMapFilterPointer = typename LabelImageToShapeLabelMapFilterType::Pointer;
		using RegionOfInterestImageFilterType        = RegionOfInterestImageFilter<IntensityImageType, OutputImageType>;
		using RegionOfInterestImageFilterPointer     = typename RegionOfInterestImageFilterType::Pointer;
		//@}

		LabelImageToShapeLabelMapFilterPointer m_LabelImageToShapeLabelMapFilter; ///< Pointer to an instance of LabelImageToShapeLabelMapFilter
		RegionOfInterestImageFilterPointer     m_RegionOfInterestFilter;          ///< Pointer to an instance of RegionOfInterestImageFilter
		LabelMapTypePointer m_LabelMapPointer; ///< Pointer to map that holds pointer to label maps
		int m_NumberOfLabels; ///< Number of labels
		int m_CurrentLabel;   ///< Current label for procressing
		
		/******* PROCESSING METHODS *******/

		/**
		*   In order to generate the split objects all labels are fed into a RegionOfInterestImageFilter.
		*   The imge is split at the region provided by the bounding box.
		*   \return void
		*/
		void GenerateData() override;

		/**
		*   Due to the multiple outputs the filter need to provide an output object. For each output object
		*   a region is allocated. The filter need to know how man y output objects are there.
		*   \return void
		*/
		virtual void GenerateOutputInformation() override;

		/**
		*   Sets the input request to the largest possible region (might be not necessary).
		*   \return void
		*/
		void GenerateInputRequestedRegion() override;

		/**
		*   Forces the output to be generated in one execution (e.g. no streaming).
		*   This method might be not necessary.
		*   \return void
		*/
		void EnlargeOutputRequestedRegion(itk::DataObject *data) override;

		/**
		 *  Each output has a different size. Therefore the individual size of the output objects must be generated
		 *  separatly.
		 *  \return void
		 */
		void GenerateOutputRequestedRegion(itk::DataObject *data) override;

		/**
		*   Compute the internal label map with the LabelImageToShapeLabelMapFilter. It sets the number of labels for the 
		*   itk::LabelImageToMultipleImageFilter::m_NumberOfLabels. The number of outputs is adjusted
		*   to the updated itk::LabelImageToMultipleImageFilter::m_NumberOfLabels. It sets the 
		*   itk::LabelImageToMultipleImageFilter::m_LabelMapPointer and resets the
		*   itk::LabelImageToMultipleImageFilter::m_CurrentLabel to zero.
		*   \return void
		*/
		void ComputeLabelMap();

		/**
		*   Print image informations. This function is not used. Use Print() instead.
		*   \param[in] os Output stream (e.g. std::cout)
		*   \param[in] indent
		*   \return void
		*/
		void PrintSelf(std::ostream& os, Indent indent) const override;

	}; // end of class
	
} // end of namespace itk


#ifndef ITK_MANUAL_INSTANTIATION
#include "itkLabelImageToMultipleImageFilter.hxx"
#endif

#endif