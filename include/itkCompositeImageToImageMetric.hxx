#ifndef itkCompositeImageToImageMetric_hxx
#define itkCompositeImageToImageMetric_hxx

#include "itkCompositeImageToImageMetric.h"

namespace itk
{
	template <typename TFixedImage, typename TMovingImage>
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::CompositeImageToImageMetric()
	{
		this->m_Submetrics.resize(1);
		this->m_SubmetricMaster = nullptr;
		this->m_SubmetricWeights.clear();
		this->m_SubmetricWeights.push_back(1.);

		this->m_Weight = 1;
		this->m_LastValue = 0;
		this->m_LastDerivative.Fill(0.0);
	}

	template <typename TFixedImage, typename TMovingImage>
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::~CompositeImageToImageMetric()
	{
		this->m_SubmetricMaster = nullptr;
		this->m_Submetrics.clear();
		this->m_SubmetricWeights.clear();

		this->m_Weight = 0;
		this->m_LastValue = 0;
		this->m_LastDerivative.Fill(0.0);
	}

	template <typename TFixedImage, typename TMovingImage>
	typename CompositeImageToImageMetric<TFixedImage, TMovingImage>::MeasureType
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetValue(const ParametersType& parameters) const
	{
		MeasureType value = 0;
		this->m_CurrentParameters = parameters;

		for(std::size_t i = 0; i < this->m_Submetrics.size(); ++i)
		{
			double IntermediateResult = this->m_Submetrics[i]->GetValue(parameters);
			IntermediateResult *= this->m_SubmetricWeights[i];
			value += static_cast<MeasureType>(IntermediateResult);
		}

		this->m_LastValue = value;
		
		return value;
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetDerivative(const ParametersType& parameters, DerivativeType& derivative) const
	{
		// TODO: Computation of the derivative.
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::SetSubmetric(SubmetricType *metric)
	{
		this->m_SubmetricMaster = metric;
	}

	template <typename TFixedImage, typename TMovingImage>
	typename CompositeImageToImageMetric<TFixedImage, TMovingImage>::SubmetricType*
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetSubmetric()
	{
		return this->m_SubmetricMaster;
	}

	template <typename TFixedImage, typename TMovingImage>
	typename CompositeImageToImageMetric<TFixedImage, TMovingImage>::SubmetricPointer
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetNthSubmetric(unsigned int n)
	{
		return this->m_Submetrics[n];
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::SetNthSubmetricWeight(unsigned int n, double weight)
	{
		if (n > this->m_Submetrics.size())
			return;
		this->m_SubmetricWeights[n] = weight;
	}

	template <typename TFixedImage, typename TMovingImage>
	double
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetNthSubmetricWeight(unsigned int n)
	{
		if (n > this->m_Submetrics.size())
			return 0.;
		return this->m_SubmetricWeights[n];
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::SetNumberOfSubmetrics(std::size_t number)
	{
		std::size_t previousNumberofMetrics = this->m_Submetrics.size();
		this->m_Submetrics.resize(number);
		this->m_SubmetricWeights.resize(number);

		// set the initial weights to 1
		if (number > previousNumberofMetrics)
		{
			for (auto i = previousNumberofMetrics; i < number; ++i)
			{
				this->m_SubmetricWeights[i] = 1;
			}
		}
	}

	template <typename TFixedImage, typename TMovingImage>
	std::size_t
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::GetNumberOfSubmetrics() const
	{
		return this->m_Submetrics.size();
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::RemoveAllSubmetrics()
	{
		this->m_Submetrics.clear();
		this->m_SubmetricWeights.clear();
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::Initialize() throw(itk::ExceptionObject)
	{
		// Clone the first metric
		if (this->m_SubmetricMaster == nullptr)
			itkExceptionMacro("No internal metric are set.");
		for (auto i = 1; i < this->GetNumberOfSubmetrics(); ++i)
		{
			this->m_Submetrics[i] = this->m_SubmetricMaster->Clone();
		}		
	}

	template <typename TFixedImage, typename TMovingImage>
	void
	CompositeImageToImageMetric<TFixedImage, TMovingImage>
	::PrintSelf(std::ostream& os, Indent indent) const
	{
		this->Superclass::PrintSelf(os, indent);

		os << indent << "Internal Metrics: (n=" << this->m_Submetrics.size() << ")\n";
		for (std::size_t i = 0; i < this->m_Submetrics.size(); i++)
		{
			if (this->m_Submetrics[i])
			{
				os << indent << i << ". "     << this->m_Submetrics[i]->GetObjectName() << "\n";
				os << indent << "   Weight: "<< this->m_SubmetricWeights[i] << "\n";
			}
		}
		os << std::endl;
	}
} // end namespace itk

#endif