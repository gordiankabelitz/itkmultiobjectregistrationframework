/*=========================================================================
*
*  Copyright Insight Software Consortium
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0.txt
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*=========================================================================*/
#ifndef itkCompositeImageToImageMetric_h
#define itkCompositeImageToImageMetric_h

#include "itkImageToImageMetric.h"
#include "vector"

namespace itk
{

	/** \class CompositeImageToImageMetric
	* \brief This class evaluates multiple submetrics for a single output value.
	*
	* The CompositeImageToImageMetric evaluates multiple submetrics for a single output value that
	* can be optimizied. The metric value are computed with an assigned weight and a concatination
	* provided by the user. If no weights and no contatination is defined all submetrics are added
	* using a weighting of 1.
	* The composite metric need a initial master metric as template for other metrics. The submetrics
	* can be change by the user with itk::CompositeImageToImageMetric::GetIthSubmetric().
	*
	* \author  Gordian Kabelitz
	* \date    03/07/2018
	* \version 0.1
	* \ingroup MultiObjectRegistrationFramework
	*/
	template<typename TFixedImage, typename TMovingImage>
	class ITK_TEMPLATE_EXPORT CompositeImageToImageMetric:
	public ImageToImageMetric<TFixedImage, TMovingImage>
	{
		
	public:
		ITK_DISALLOW_COPY_AND_ASSIGN(CompositeImageToImageMetric);

		//@{
		/** Standard class type aliases */
		using Self         = CompositeImageToImageMetric;
		using Superclass   = ImageToImageMetric<TFixedImage, TMovingImage>;
		using Pointer      = SmartPointer<Self>;
		using ConstPointer = SmartPointer<const Self>;
		//@}

		//@{
		/** Standard class macro */
		itkNewMacro(Self);
		itkTypeMacro(CompositeImageToImageMetric, ImageToImageMetric);
		//@}

		//@{
		/** Class definitions */
		using SubmetricType         = Superclass;
		using SubmetricPointer      = typename Superclass::Pointer;
		using SubmetricConstPointer = typename Superclass::ConstPointer;

		using ParametersType = typename Superclass::ParametersType;
		using MeasureType    = typename Superclass::MeasureType;
		using DerivativeType = typename Superclass::DerivativeType;
		//@}

		/** 
		 * Return the cost function value specified by the parameters and composed by the values 
		 * of the submetrics weighted by their individual weights (default 1).
		 * \param[in] parameters Reference to parameters that needs to be evaluated
		 * \return Value of cost function at parameters
		*/
		MeasureType GetValue(const ParametersType &parameters) const override;

		/**
		* Return the cost function derivative composed by the values of the submetrics
		* weighted by their individual weights (default 1).
		* \param[in] parameters Reference to parameters that needs to be evaluated
		* \param[out] derivative Derivative at given parameters
		* \return void
		*/
		void GetDerivative(const ParametersType &parameters, DerivativeType &derivative) const override;
		
		/**
		* Set the metric type of the submetrics. All metrics will be set to this metric type
		* at the initialzation of the composite metric. The metric master will be provided
		* by the MultiObjectregistrationFramework.
		* All metric that are based on the itk:ImageToImageMetric are valid.
		* \param metric Pointer to metric of base class itk::ImageToImageMetric
		* \return void
		**/
		void SetSubmetric(SubmetricType *metric);

		/**
		* Get the metric master that will be cloned in the initialization phase to the other submetrics.
		* \return Raw pointer to submetric at itk::CompositeImageToImageMetric::m_Submetrics
		*/
		SubmetricType* GetSubmetric();


		/**
		* Return a pointer to a selected submetric.
		* \param n Index of the selected submetric
		* \return Pointer to ith submetric
		*/
		SubmetricPointer GetNthSubmetric(unsigned int n);

		/**
		 * Assigns a weight to an submetric. This metric is weighted against the other metrics with this weight.
		 * \param n Index of submetric
		 * \param weight Weight for the submetric
		 * \return void
		*/
		void SetNthSubmetricWeight(unsigned int n, double weight);

		/**
		 * Get the weight of the nth submetric. The weight is not changed during any processing
		 * (no normalization). If the index is higher than the number of submetrics 0 is returned.
		 * \param n Index of submetric
		 * \return Weight of queried submetric
		 */
		double GetNthSubmetricWeight(unsigned int n);

		/** Set number of submetric. Usually the number of submetrics are the same
		 * as the number of fixed input images. The metric vector will be resized with the
		 * vector::resize() function. The new vector elements are initialized with nullptr.
		 * The internal weight vector is changed accordingly. New metrics will be weighted with 1.
		 * This can be changed by SetSubmetricWeight(index, weight).
		 * \param number New number of internal metrics
		*/
		void SetNumberOfSubmetrics(std::size_t number);

		/** 
		 * Return the number of submetrics. This returns the size of the submetric vector.
		 * Some of the vector elements may be nullptr.
		 * \return Number of submetrics
		*/
		std::size_t GetNumberOfSubmetrics() const;

		/**
		 * Clears the submetric vector as well as the submetric weights.
		 * \return void
		*/
		void RemoveAllSubmetrics();

		/**
		* Get weight of this composite metric object
		* \return Weight of this metric instance
		*/
		itkGetMacro(Weight, double);

		/**
		* Get the last stored values computed by itk::CompositeImageToImageMetric::GetValue()
		* \return itk::CompositeImageToImageMetric::MeasureType
		*/
		itkGetMacro(LastValue, MeasureType);

		/**
		* Get the last stored derivates computed by itk::CompositeImageToImageMetric::GetDerivative()
		* \return itk::CompositeImageToImageMetric::DerivativeType
		*/
		itkGetMacro(LastDerivative, DerivativeType);

		/**
		 * Initialize the submetrics. The first metric must be set using SetSubmetric(). To this point just new metrics
		 * are created the initialization of the member variables is still open.
		 * \todo Copy member variables from the master metric to the other metrics.
		 * \return void
		**/
		void Initialize() throw (itk::ExceptionObject) override;

		//@{
		/** Constructor and Destructor */
		CompositeImageToImageMetric();
		virtual ~CompositeImageToImageMetric();
		//@}

	protected:
		/**
		 * Pointer to the master submetric that will be cloned at initialization. This metric
		 * will replace all submetrics that are nullptr.
		 */
		SubmetricConstPointer m_SubmetricMaster;

		 /**
		  * Vector for the submetrics that contribute to the composite metric value.
		  * As for now they are initialize with the same metric type.
		 */
		std::vector<SubmetricPointer> m_Submetrics;

		/** Vector with weights for each submetric */
		std::vector<double> m_SubmetricWeights;

		/** Current parameters that are shared with the submetrics */
		mutable ParametersType m_CurrentParameters;

		/** Weight of this composite metric against the other composite metrics */
		double m_Weight;

		/** Last computed composite value */
		mutable MeasureType m_LastValue;

		/** Last computed derivative value */
		mutable DerivativeType m_LastDerivative;

		/**
		*   Print image informations. This function is not used. Use Print() instead.
		*   \param[in] os Output stream (e.g. std::cout)
		*   \param[in] indent
		*   \return void
		*/
		void PrintSelf(std::ostream &os, Indent indent) const;

	};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkCompositeImageToImageMetric.hxx"
#endif
#endif
