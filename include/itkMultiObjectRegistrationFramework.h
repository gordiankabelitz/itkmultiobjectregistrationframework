/*=========================================================================
*
*  Copyright Insight Software Consortium
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0.txt
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*=========================================================================*/
#ifndef itkMultiObjectRegistrationFramework_h
#define itkMultiObjectRegistrationFramework_h

// ITK includes
#include "itkDataObjectDecorator.h"
#include "itkEventObject.h"
#include "itkProcessObject.h"
#include "itkImageToImageMetric.h"
#include "itkSingleValuedNonLinearOptimizer.h"
#include "itkObjectFactory.h"
#include "itkTransform.h"
#include "itkTileImageFilter.h"

// RTK includes
#include "rtkThreeDCircularProjectionGeometry.h"
#include "rtkCudaForwardProjectionImageFilter.h"

// Internal includes
#include "itkLabelImageToMultipleImageFilter.h"
#include "itkCompositeImageToImageMetric.h"

// STL includes
#include <vector>

namespace itk
{
	/** Custom events **/
	itkEventMacroDeclaration(StartMultiObjectRegistration, itk::AnyEvent);

	/**
	* \class MultiObjectRegistrationFramework  
	* \brief Framework that independently registers different objects.
	*
	* This class is templated over the provided fixed images, the 3D volume of the moving objects
	* and the 2D projections of these objects.
	* The components needed are:
	*	- ImageToImageMetricType
	*	- OptimizerType
	*	- TransformType
	*	- InterpolatorType
	*	
	*	n - index for moving object sets
	*	m - index for fixed image sets
	*	
	* For each MovingObjectSet a itk::CompositeImageToImageMetric is created.
	* \todo Cast each image internally to a itk::CudaImage for use with CudaForwardProjector?
	* 
	* \author Gordian Kabelitz
	* \version 0.1
	* \date 04/07/2018
	* \ingroup ITKRegistrationMethods
	* \ingroup MultiObjectRegistrationFramework
	*/
	template<typename TFixed2DImage,
		     typename TMoving3DImage,
			 typename TVirtualImage = TFixed2DImage>
	class ITK_TEMPLATE_EXPORT MultiObjectRegistrationFramework
		: public ProcessObject
	{

	public:
		ITK_DISALLOW_COPY_AND_ASSIGN(MultiObjectRegistrationFramework);

		//@{
		/** Standard class type aliases */
		using Self         = MultiObjectRegistrationFramework;
		using Superclass   = ProcessObject;
		using Pointer      = SmartPointer<Self>;
		using ConstPointer = SmartPointer<const Self>;
		//@}

		//@{
		/** Standard class macro */
		itkNewMacro(Self);
		itkTypeMacro(MultiObjectRegistrationFramework, ProcessObject);
		//@}

		//@{
		/** Class definitions derived from template */
		using Fixed2DImageType         = TFixed2DImage;
		using Fixed2DPixelType         = typename Fixed2DImageType::PixelType;
		using Fixed2DImagePointer      = typename Fixed2DImageType::Pointer;
		using Fixed2DImageConstPointer = typename Fixed2DImageType::ConstPointer;
		using Fixed2DImageRegionType   = typename Fixed2DImageType::RegionType;

		using VirtualImageType        = TVirtualImage;
		using VirtualImagePixelType   = typename VirtualImageType::PixelType;
		using VirtualImagePointer     = typename VirtualImageType::Pointer;
		using VirtualImageRegionType  = typename VirtualImageType::RegionType;

		using Moving3DImageType        = TMoving3DImage;
		using Moving3DPixelType        = typename Moving3DImageType::PixelType;
		using Moving3DImagePointer     = typename Moving3DImageType::Pointer;
		using Moving3DImageRegionType  = typename Moving3DImageType::RegionType;

		static constexpr unsigned int Image2DDimension = TFixed2DImage::ImageDimension;
		static constexpr unsigned int Image3DDimension = TMoving3DImage::ImageDimension;
		//@}

		//@{
		/** Input type alias for the transforms */
		using RealType                    = double;
		using InitialTransformType        = MatrixOffsetTransformBase<RealType, Image3DDimension, Image3DDimension>;
		using InitialTransformPointer     = typename InitialTransformType::Pointer;
		using InitialParametersType       = typename InitialTransformType::ParametersType;

		using TransformType               = Transform<RealType, Image3DDimension, Image3DDimension>;
		using TransformPointer            = TransformType*;
		using TransformParametersType     = typename TransformType::ParametersType;

		using TransformOutputType         = DataObjectDecorator<TransformType>;
		using TransformOutputPointer      = typename TransformOutputType::Pointer;
		using TransformOutputConstPointer = typename TransformOutputType::ConstPointer;
		//@}

		//@{
		/** Type aliases for projections */
		using ProjectionGeometryType      = rtk::ThreeDCircularProjectionGeometry;
		using ProjectionPointer           = ProjectionGeometryType::Pointer;
		using ProjectionConstPointer      = ProjectionGeometryType::ConstPointer;

		using DRREngineType               = rtk::CudaForwardProjectionImageFilter<Moving3DImageType, Moving3DImageType>;
		using DRREnginePointer            = typename DRREngineType::Pointer;
		using DRREngineOutputType         = typename DRREngineType::OutputImageType;
		//@}

		//@{
		/** Metrics for moving to fixed images */
		using CompositeMetricType         = CompositeImageToImageMetric<Fixed2DImageType, VirtualImageType>;
		using CompositeMetricPointer      = typename CompositeMetricType::Pointer;
		using BaseMetricType              = ImageToImageMetric<Fixed2DImageType, VirtualImageType>;
		using BaseMetricPointer           = BaseMetricType*;
		using MetricMeasureType           = typename BaseMetricType::MeasureType;
		using MetricDerivateType          = typename BaseMetricType::DerivativeType;
		//@}

		//@{
		/** Type of optimizer for composite metric */
		using OptimizerType               = SingleValuedNonLinearOptimizer;
		using OptimizerPointer            = OptimizerType*;
		//@}

		//@{
		/** Constructor and Destructor */
		MultiObjectRegistrationFramework();
		~MultiObjectRegistrationFramework();
		//@}

		/******* INPUT HANDLING *******/

		/**
		* Add a moving 3D object to the registration process. This object is an already segmented object
		* in a bounding box. This object will be part of a single registration process. The new elements
		* is added behind the last position. For each added object the following components are created.
		* InitialTransform (is IdentityTransform if not otherwise specified), internal transform, composite
		* metric and optimizer. All put in corresponding vector at the same index.
		* An output object is created using GetOutput()
		* \param[in] object Object to be registered
		* \return void
		*/
		void AddMovingObject(const Moving3DImagePointer object);

		/**
		* Access to the nth moving object. If n is larger than size of container this method returns
		* a nullptr.
		* \param[in] n Index of the queried object
		* \return Pointer to the moving object
		*/
		Moving3DImagePointer GetNthMovingObject(unsigned int n);

		/**
		 * Erase a single object from the object container. The corresponding initial transform and other
		 * dependent elements are erased as well.
		 * \param[in] n Index of object to be erased
		 * \return void
		 */
		void RemoveNthMovingObject(unsigned int n);

		/**
		* Get number of moving objects. Does not provide the number of provided initial transforms.
		* \return Number of moving objects
		*/
		std::size_t GetNumberOfObjects() const;

		/**
		* Initial transform for the nth moving object. This transform will be applied before the
		* registration process. The initial transform is going to be composed with the output
		* transform. To set an initial transform the corresponding object needs to be set. If
		* no initial transformation is set an identity transform is assumed.
		* \param[in] n Index of the according moving image
		* \param[in] transform Initial Transform for an moving object
		* \return void
		*/
		void SetNthInitialTransform(unsigned int n, const InitialTransformPointer transform);

		/**
		* Access to the initial transform for the nth moving object. If n is larger than the
		* container a nullptr is returned.
		* \param[in] n Index of the initial transform
		* \return Pointer to the queried inital transform
		*/
		InitialTransformPointer GetNthInitialTransform(unsigned int n) const;

		/**
		* Convenience method for adding moving object and initial transformation at once. Both are
		* are added behind the last element.
		* \see itk::MultiObjectRegistrationFramework::AddMoving3DObject()
		* \see itk::MultiObjectRegistrationFramework::SetNthInitialTransform()
		* \param[in] object Object to be registered
		* \param[in] transform Initial Transform for an moving object
		* \return void
		*/
		void AddMovingObjectWithInitialTransform(const Moving3DImagePointer object, const InitialTransformPointer transform);

		/**
		 * Remove all moving objects and their initial transformations
		 * \return void
		 */
		void RemoveAllMovingObjectsAndInitialTransforms();
		
		/**
		* Add a fixed projection image to the registration process. The fixed image resembles a flat
		* panel detector. Each fixed image needs a projection geometry. The indices of the fixed images
		* are independent from the moving objects. The fixed image and the projection geometry are added
		* behind the last container element. A new DRR engine container is created with a nullptr engine.
		* \param[in] fixedimage Fixed projection image
		* \param[in] geometry Projection Geometry for the fixed image
		* \return void
		*/
		void AddFixedImageAndGeometry(const Fixed2DImagePointer fixedimage, const ProjectionPointer geometry);
	
		/**
		* Access to the mth fixed projection image. If m is larger than the fixed image container
		* a nullptr is returned.
		* \param[in] m Index of the queried fixed image
		* \return Pointer to the queried fixed image
		*/
		Fixed2DImagePointer GetMthFixedImage(unsigned int m);

		/**
		 * Remove the mth fixed image. The corresponding projection is removed aswell. If m is larger
		 * than the fixed image container nothing is removed.
		 * \param m Index of the fixed image/projection to be removed
		 * \return void
		 */
		void RemoveMthFixedImage(unsigned int m);

		/**
		* Get number of fixed images. Does not provide the number of provided projection geometries
		* (but should be the same).
		* \return Number of fixed projection images
		*/
		std::size_t GetNumberOfFixedImages() const;

		/**
		* Set an alternative projection geometry for the mth image. This function should only be used
		* for a projection geometry update due to calibration. 
		* \param[in] m Index of the corresponding fixed image
		* \param[in] geometry Updated projection Geometry for the fixed image
		* \return void
		*/
		void SetMthProjectionGeometry(unsigned int m, const ProjectionPointer geometry);

		/**
		* Access to the mth fixed projection geometry. If m is larger than the number of DRREngine
		* a nullptr is returned.
		* \param[in] m Index of the queried projection geometry
		* \return Pointer to the queried projection geometry
		*/
		ProjectionPointer GetMthProjectionGeometry(unsigned int m);

		/**
		* Remove all fixed images and projection geometries.
		* \return void
		*/
		void RemoveAllFixedImagesAndProjectionGeometries();

		/******* COMPONENT HANDLING *******/

		/**
		*   Sets the DRR engine for the registration process. This engine is the master engine and
		*   all of the parameters will be cloned to the other subregistration processes. To replace 
		*   a particular engine or the parameter of a particular engine see
		*   SetMthDRREngine() or GetMthDRREngine()
		*   \param[in] drrEngine Pointer to the drrEngine
		*   \return void
		*/
		void SetDRREngineMaster(DRREnginePointer drrEngine);

		/**
		*   Get the pointer to the DRR engine master.
		*   \return Pointer to DRR engine master
		*/
		DRREnginePointer GetDRREngineMaster();

		/**
		*   Set the interpolation engine for the mth object. The interpolation in this case is the projection
		*   from the 3D object space to the 2D projection space.
		*   \param[in] m Index of the mth DRR engine
		*   \param[in] drrEngine Pointer to the drrEngine
		*   \return void
		*/
		void SetMthDRREngine(unsigned int m, DRREnginePointer drrEngine);

		/**
		*   Get the pointer to the mth projection engine.
		*   \param[in] m Index of the queried DRR engine
		*   \return Pointer to the mth DRREngine
		*/
		DRREnginePointer GetMthDRREngine(unsigned int m);

		/**
		*   Set the submetric type for the all composite image to image metrics. The metric will be cloned
		*   for all subcomponents.
		*   \see itk::CompositeMetric::Initialize()
		*   \param[in] metric Pointer to an image to image metric
		*   \return void
		*/
		void SetMetricMaster(BaseMetricPointer metric);

		/**
		*  Get the Pointer to the metric master.
		*  \return Pointer to metric master
		*/
		BaseMetricPointer GetMetricMaster();

		/**
		*   Get the metric of the nth composite image to image metric. All sub components share
		*   this metric type. 
		*   \todo Why does this return a raw pointer instead of a smartpointer?
		*   \param[in] n index of the nth composite metric
		*   \return Raw pointer to the nth composite metric
		*/
		BaseMetricPointer GetNthCompositeMetric(unsigned int n);

		/**
		 * Set the transform master for the registration process. All transform components will be initialized
		 * with this transform (including parameters).
		 * \param[in] transform Pointer to a transform
		 * \return void
		 */
		void SetTransformMaster(TransformPointer transform);

		/**
		 * Get pointer to master transform
		 * \return Pointer to master transform
		 */
		TransformPointer GetTransformMaster();

		/**
		 * Set the nth transformation. This methods can be used to replace a certain transformation
		 * to match the physical properties of the object (e.g. liver allows for deformable
		 * transformation, the properties of bones usually satisfied with a rigid transformation).
		 * The TransformMaster cannot be set with this method: SetTransformMaster()
		 * \param[in] n Index of the transform to be replaced
		 * \param[in] transform Transform pointer to the replacing transform
		 * \return void
		 */
		void SetNthTransform(unsigned int n, TransformPointer transform);

		/**
		 * Get the nth transform pointer. This method does not return the TransformMaster. If n is
		 * larger than the transformation vector a nullptr is returned.
		 * \param[in] n Index of the quiered transformation
		 * \return Pointer to the nth transform
		 */
		TransformPointer GetNthTransform(unsigned int n);

		/**
		 * Set the master optimizer for the registration process. This optimizer type will be cloned
		 * for all optimizer including scales.
		 * \param[in] optimizer Pointer to master transform 
		 */
		void SetOptimizerMaster(OptimizerPointer optimizer);

		/**
		* Get pointer to master optimizer. 
		* \return Pointer to master optimizer
		*/
		OptimizerPointer GetOptimizerMaster();

		/**
		 * Set the optimizer for the nth object. This changes the optimizing strategy for this
		 * metric. The set optimizer wont be replaced by the optimizer master. The optimizer
		 * properties have to be set individually.
		 * \param[in] n Index of optimizer to be replaced
		 * \param[in] optimizer Replacing optimizer
		 * \return void
		 */
		void SetNthOptimizer(unsigned int n, OptimizerPointer optimizer);

		/**
		 * Get the nth optimizer pointer. This method does not return the optimizer master.
		 * If n is larger than the optimizer vector a nullptr is returned.
		 * \param[in] n Index of quiered optimizer
		 * \return Pointer to optimizer
		 */
		OptimizerPointer GetNthOptimizer(unsigned int n);

		/******* INITIALIZATION *******/

		/**
		 * Initialize the registration framework. First check for MC. It is recommended to enclose
		 * this method in a try-catch block. This method sets up the provided
		 * components and the user-specified inputs will be handled in the subroutines. 
		 * This method invokes several initialization methods for the components.
		 * \see itk::MultiObjectRegistrationFramework::InitializeDRRComponents()
		 * \see itk::MultiObjectRegistrationFramework::InitializeTransformComponents()
		 * \see itk::MultiObjectRegistrationFramework::InitializeCompositeMetricComponents()
		 * \see itk::MultiObjectRegistrationFramework::InitializeOptimizerComponents()
		 * \return True if all components are set up correctly, False otherwise
		 */
		bool Initialize() throw (itk::ExceptionObject);

		/**
		* Initialize the DRR subcomponents. To each FixedImageSet a DRR engine is provided. The
		* outputs of the DRR engine are connected to the metric inputs. This methods needs to
		* be called whenever the number of fixed images or objects are changed.
		* \return void
		*/
		void InitializeDRRComponents() throw (itk::ExceptionObject);

		/**
		* Initialize transformation subcomponents. This methods needs to be called whenever a
		* transformation changed during the registration process.
		* \return void
		*/
		void InitializeTransformComponents() throw (itk::ExceptionObject);

		/**
		* Initialize the composite metric subcomponents. The composite metrics itself initialize
		* their submetrics. This methods needs to be called whenever a transformation changed
		* during the registration process.
		* \return void
		*/
		void InitializeCompositeMetricComponents() throw (itk::ExceptionObject);

		/**
		* Initialize the optimizer subcomponents. For each composite metric an optimizer is needed.
		* Whenever a optimizer component is changed this methods should be called to update the
		* internal connections.
		* \return True if all components are set up correctly, False otherwise
		*/
		void InitializeOptimizerComponents() throw (itk::ExceptionObject);
		
		/******* OUTPUT HANDLING *******/

		/**
		* Returns the output transformation after the registration process is finished. The output
		* transform is composed with the initial transform. For each moving object an output transform
		* is generated.
		* \todo Compose OutputTransform with Initial Transform
		* \param[in] n Index of the transform for the queried object
		* \return Pointer to the nth output transform
		*/
		const TransformOutputType* GetOutput(unsigned int n) const;
	
	protected:

		/******* INPUT MEMBER *******/

		/** Vector of n moving 3D objects. DRRs are computed from this objects. */
		std::vector<Moving3DImagePointer>    m_MovingObjects;
		/** Vector of initial transforms for the moving objects. Indices correspond to the object vector. */
		std::vector<InitialTransformPointer> m_InitialTransforms;
		/** Counter that keeps the current MovingObjectSet. */
		unsigned int m_CurrentMovingObjectSet;

		/** Vector of m fixed images (reference images) after they are expanded to the third dimension. */
		std::vector<Fixed2DImagePointer>     m_FixedImages;
		/** Vector of m projection geometries of the fixed images. Indices correspond to the fixed image vector. */
		std::vector<ProjectionPointer>       m_ProjectionGeometries;
		/** Counter that keeps the current FixedImageSet .*/
		unsigned int m_CurrentFixedImageSet;
		
		/******* REGISTRATION COMPONENT MEMBER *******/

		//@{
		/** Master or base components */
		DRREnginePointer  m_DRREngineMaster;
		OptimizerPointer  m_OptimizerMaster;
		BaseMetricPointer m_MetricMaster;
		TransformPointer  m_TransformMaster;

		//@}

		/** Vector of n*m (GPU-based) DRR engines for projection of the objects. */
		std::vector<DRREnginePointer>  m_DRREngines;
		/** Vector of m composite image metrics. Each composite metric own n submetrics.
		 * \see itk::CompositeImageToImageMetric
		 */
		std::vector<CompositeMetricPointer> m_CompositeMetrics;
		/** Vector of n transformations, one for each object. This transformations are updated by the subtransforms. */
		std::vector<TransformPointer>  m_Transforms;
		/** Vector of n optimizers that try to optimize the composite metric. */
		std::vector<OptimizerPointer>  m_Optimizers;

		/******* PROCESSING FLAGS *******/

		/** Flag to stop the registration process. */
		bool m_Stop;

		/******* INTERNAL PROCESS IMAGE FILTER *******/

		
	
		/******* PROCESSING METHODS *******/

		/**
		* Make DataObjects of the specified type as output. The specified type is a transform.
		* If n is larger than the number of moving objects a nullptr is returned.
		* \param[in] n Index of the transform for the nth object
		* \return Pointer to the output transform for the nth object
		*/
		DataObjectPointer MakeOutput(unsigned int n);

		/**
		* This method invokes the registration pipeline. 
		* \return void
		*/
		void GenerateData() throw (itk::ExceptionObject);

		/**
		* Add internal observers for monitoring the metrics.
		* \return void
		*/
		void AddInternalObservers();

		/**
		* Remove all internal observers for monitoring the metrics.
		* \return void
		*/
		void RemoveInternalObservers();

		/******* INFORMATION METHODS *******/

		/**
		*   Print image informations. This function is not used. Use Print() instead.
		*   \param[in] os Output stream (e.g. std::cout)
		*   \param[in] indent
		*   \return void
		*/
		void PrintSelf(std::ostream& os, Indent indent) const;

	};
	
} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkMultiObjectRegistrationFramework.hxx"
#endif

#endif
