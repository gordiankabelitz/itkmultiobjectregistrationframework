#include "itkLabelImageToMultipleImageFilter.h"
#include "itkImageFileWriter.h"
#include <cstdlib>
#include "itkTestingMacros.h"


const signed int Dim = 3;
using Pixeltype = float;
using ImageType = itk::Image<Pixeltype, Dim>;
using ImagePointer = ImageType::Pointer;

using LabelImageToMultipleImageFilterType = itk::LabelImageToMultipleImageFilter<ImageType>;
using LabelImagePointer = LabelImageToMultipleImageFilterType::Pointer;

void CreateImage(ImagePointer);

int itkLabelImageToMultipleImageFilterTest(int , char*[])
{
	/** Create a test image with some cubes */
	auto image = ImageType::New();
	CreateImage(image);
	auto TestImageWriter = itk::ImageFileWriter<ImageType>::New();
	TestImageWriter->SetFileName("TestImage.nrrd");
	TestImageWriter->SetInput(image);
	TestImageWriter->Update();

	auto lok = true;
	auto okay = true;

	/** Put image into filter */
	auto LabelImageFilter = LabelImageToMultipleImageFilterType::New();

	/** Non labels are set. Therefore no current label and zero labels */
	if (LabelImageFilter->GetCurrentLabel() != -1)
		lok = false;
	if (LabelImageFilter->GetNumberOfLabels() != 0)
		lok = false;

	/** Test print method */
	LabelImageFilter.Print(std::cout);

	/** Set input to filter */
	try
	{
		LabelImageFilter->SetInput(image);
	}
	catch (...)
	{
		lok = false;
	}

	/** Check member variables after input */
	if (LabelImageFilter->GetNumberOfLabels() != 4)
		lok = false;
	if (LabelImageFilter->GetCurrentLabel() != 0)
		lok = false;

	okay = lok && okay;
	if (!lok)
		std::cout << "Read error" << std::endl;

	/** OUTPUT CHECK */
	lok = true;
	try
	{
		LabelImageFilter->Update();
	}
	catch (itk::ExceptionObject &EO)
	{
		EO.Print(std::cout);
		lok = false;
	}

	okay = lok && okay;
	if (!lok)
		std::cout << "Process error" << std::endl;

	/** Check pipeline functionality */
	lok = true;
	auto FileWriter = itk::ImageFileWriter<ImageType>::New();
	std::string filenamebase("ROI");

	for (auto i = 0; i < LabelImageFilter->GetNumberOfLabels(); ++i)
	{
		auto filename = filenamebase.append(std::to_string(i));
		filename.append(".nrrd");
		FileWriter->SetInput(LabelImageFilter->GetOutput(i));
		FileWriter->SetFileName(filename);
		try
		{
			FileWriter->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			EO.Print(std::cout);
			lok = false;
		}
	}
	if (!lok)
		std::cout << "Write error" << std::endl;
	okay = lok && okay;

	/** Label are not destroyed with filter destruction */
	lok = true;
	auto LabelObjects = std::vector<ImagePointer>();
	for (auto i = 0; i < LabelImageFilter->GetNumberOfLabels(); ++i)
	{
		LabelObjects.push_back(LabelImageFilter->GetOutput(i));
	}

	LabelImageFilter = nullptr;

	TEST_SET_GET_VALUE(4, LabelObjects.size());
	for (auto i = 0; i < LabelObjects.size(); ++i)
	{
		if (LabelObjects[i] == nullptr)
			lok = false;
		else
			LabelObjects[i]->Print(std::cerr);
	}
	
	okay = lok && okay;

	// Final check
	if (okay)
	{
		return EXIT_SUCCESS;
	}
		return EXIT_FAILURE;
}

void
CreateImage
(ImagePointer Image)
{
	ImageType::IndexType idx;
	idx.Fill(0);
	ImageType::SizeType sz;
	sz.Fill(200);

	ImageType::RegionType reg(idx, sz);
	Image->SetRegions(reg);
	Image->Allocate();
	Image->FillBuffer(0);

	for (unsigned int r = 20; r < 80; r++)
	{
		for (unsigned int c = 30; c < 100; c++)
		{
			for (unsigned int d = 40; d < 80; d++)
			{
				ImageType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 1);
			}
		}
	}

	// Set pixels in a different square to a different value
	for (unsigned int r = 100; r < 130; r++)
	{
		for (unsigned int c = 115; c < 160; c++)
		{
			for (unsigned int d = 115; d < 170; d++)
			{
				ImageType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 2);
			}
		}
	}

	int center[] = {60,70,150};
	auto radius = 20;
	for (unsigned int r = 40; r < 80; r++)
	{
		for (unsigned int c = 50; c < 90; c++)
		{
			for (unsigned int d = 130; d < 170; d++)
			{
				auto r_c = (r - center[0]) * (r - center[0]);
				auto c_c = (c - center[1]) * (c - center[1]);
				auto d_c = (d - center[2]) * (d - center[2]);
				if (sqrt(r_c + c_c + d_c) < radius)
				{
					ImageType::IndexType pixelIndex;
					pixelIndex[0] = r;
					pixelIndex[1] = c;
					pixelIndex[2] = d;

					Image->SetPixel(pixelIndex, 3);
				}
			}
		}
	}
}
