#include "itkMultiObjectRegistrationFramework.h"
#include <cstdlib>
#include "itkTestingMacros.h"

// ITK
#include "itkTransform.h"
#include "itkEuler3DTransform.h"
#include "itkImageFileWriter.h"
#include "itkExtractImageFilter.h"
#include "itkMeanSquaresImageToImageMetric.h"
#include "itkNormalizedMutualInformationHistogramImageToImageMetric.h"
#include "itkBSplineTransform.h"
#include "itkGradientDescentOptimizer.h"
#include "itkOnePlusOneEvolutionaryOptimizer.h"
#include "itkPowellOptimizer.h"

// RTK
#include "rtkCudaForwardProjectionImageFilter.h"


const unsigned int Dim3 = 3;
const unsigned int Dim2 = 2;
using Pixeltype = float;
using Image3DType    = itk::CudaImage<Pixeltype, Dim3>;
using Image3DPointer = Image3DType::Pointer;
using Image2DType    = itk::CudaImage<Pixeltype, Dim2>;
using Image2DPointer = Image2DType::Pointer;

using TransformType = itk::Euler3DTransform<double>;
using TransformPointer = TransformType::Pointer;

using ProjectorType = rtk::CudaForwardProjectionImageFilter<Image3DType>;
using ProjectorPointer = ProjectorType::Pointer;

using ImageWriterType = itk::ImageFileWriter<Image3DType>;

/** Helper functions */
/** Creates an image with three label objects */
void
CreateObjectImage(Image3DPointer);
/** Provides a transform matrix with rotations about (x,y,z)-axis in radians */
TransformType::MatrixType
CreateRotationTransform(double x, double y,double z);
/** Create a projector image (= detector) */
void
CreateProjectorImage(Image3DPointer, int DetectorR, int DetectorC, double SpacingR = 1, double SpacingC = 1);
/** Writes image to test folder */
void
WriteImage(Image3DPointer, std::string);

/***********************************************************/
/********************   TEST   *****************************/
/***********************************************************/
int itkMultiObjectRegistrationFrameworkTest(int , char*[])
{
	std::cerr << "INPUT HANDLING" << std::endl;
	std::cerr << " * Create volume with objects and split volume into multiple sub images" << std::endl;
	auto ObjectImage = Image3DType::New();
	CreateObjectImage(ObjectImage);
	WriteImage(ObjectImage, "ObjectImage");

	auto LabelImageToMultipleImageFilter = itk::LabelImageToMultipleImageFilter<Image3DType>::New();
	LabelImageToMultipleImageFilter->SetInput(ObjectImage);
	
	auto ObjectVector = std::vector<Image3DPointer>();
	// i = 1 because 0 is the background label
	for (auto i = 1; i < LabelImageToMultipleImageFilter->GetNumberOfLabels(); ++i)
	{
		ObjectVector.push_back(LabelImageToMultipleImageFilter->GetOutput(i));
	}

	std::cerr << " * Create MultiObjectregistrationObject" << std::endl;
	auto ObjectRegistration = itk::MultiObjectRegistrationFramework<Image2DType, Image3DType>::New();
	EXERCISE_BASIC_OBJECT_METHODS(ObjectRegistration, MultiObjectRegistrationFramework, ProcessObject);

	TEST_SET_GET_VALUE(0, ObjectRegistration->GetNumberOfObjects());
	TEST_SET_GET_VALUE(0, ObjectRegistration->GetNumberOfFixedImages());

	auto IdTransform = itk::MatrixOffsetTransformBase<double>::New();
	IdTransform->SetIdentity();
	for (auto i = 0; i < ObjectVector.size(); ++i)
	{
		ObjectRegistration->AddMovingObject(ObjectVector[i]);
		TEST_SET_GET_VALUE(i+1, ObjectRegistration->GetNumberOfObjects());
		TEST_SET_GET_VALUE(IdTransform->GetMatrix(), ObjectRegistration->GetNthInitialTransform(i)->GetMatrix());
		TEST_SET_GET_VALUE(ObjectVector[i], ObjectRegistration->GetNthMovingObject(i));
	}

	std::cerr << " * Add non identity transform to object" << std::endl;
	auto Transform = itk::MatrixOffsetTransformBase<double>::New();
	double Rotation[] = { 0.1, 0, 0.2 };
	Transform->SetMatrix(CreateRotationTransform(Rotation[0], Rotation[1], Rotation[2]));
	ObjectRegistration->SetNthInitialTransform(1, Transform);
	TEST_SET_GET_VALUE(CreateRotationTransform(Rotation[0], Rotation[1], Rotation[2]), ObjectRegistration->GetNthInitialTransform(1)->GetMatrix());

	std::cerr << " * Remove an object from the object list" << std::endl;
	ObjectRegistration->RemoveNthMovingObject(0);
	TEST_SET_GET_VALUE(2, ObjectRegistration->GetNumberOfObjects());

	std::cerr << " * Add the removed object again with a different method. The object is now the last element" << std::endl;
	auto Transform2 = itk::MatrixOffsetTransformBase<double>::New();
	Transform2->SetMatrix(CreateRotationTransform(Rotation[0], Rotation[1], Rotation[2]));
	ObjectRegistration->AddMovingObjectWithInitialTransform(ObjectVector[0], Transform2);
	TEST_SET_GET_VALUE(3, ObjectRegistration->GetNumberOfObjects());
	TEST_SET_GET_VALUE(CreateRotationTransform(Rotation[0], Rotation[1], Rotation[2]), ObjectRegistration->GetNthInitialTransform(2)->GetMatrix());

	std::cerr << " * Remove all objects and initial transforms" << std::endl;
	ObjectRegistration->RemoveAllMovingObjectsAndInitialTransforms();
	TEST_SET_GET_VALUE(0, ObjectRegistration->GetNumberOfObjects());

	std::cerr << " * Add the objects and initial transforms" << std::endl;
	for (auto i = 0; i < ObjectVector.size(); ++i)
	{
		ObjectRegistration->AddMovingObject(ObjectVector[i]);
	}
	TEST_SET_GET_VALUE(ObjectVector.size(), ObjectRegistration->GetNumberOfObjects());

	std::cerr << " * Create fixed images and projection geometries" << std::endl;
	std::vector<rtk::ThreeDCircularProjectionGeometry::Pointer> Geometries;
	std::vector<ProjectorPointer> Projectors;
	std::vector<Image3DPointer> Projections;
	for (auto i = 0; i < 3; ++i)
	{
		auto Geometry = rtk::ThreeDCircularProjectionGeometry::New();
		Geometry->AddProjection(400, 800, i * 30);
		auto Projector = ProjectorType::New();
		Projector->SetGeometry(Geometry);
		Projector->SetInput(1, ObjectImage);
		auto Projection = Image3DType::New();
		CreateProjectorImage(Projection, 500, 400);
		Projector->SetInput(Projection);
		TRY_AND_EXIT_ON_ITK_EXCEPTION(Projector->Update());

		Geometries.push_back(Geometry);
		Projectors.push_back(Projector);
		Projections.push_back(Projector->GetOutput());
	}
	WriteImage(Projections[0], "Projection0");
	WriteImage(Projections[1], "Projection1");
	WriteImage(Projections[2], "Projection2");

	std::vector<Image2DPointer> Projections2D;
	for (auto i = 0; i < Projections.size(); ++i)
	{
		Image3DType::IndexType ExtractionIndex;
		ExtractionIndex.Fill(0);
		auto ExtractionSize = Projections[i]->GetLargestPossibleRegion().GetSize();
		ExtractionSize[2] = 0;
		Image3DType::RegionType ExtractionRegion(ExtractionIndex, ExtractionSize);
		auto Extractor = itk::ExtractImageFilter<Image3DType, Image2DType>::New();
		Extractor->SetInput(Projections[i]);
		Extractor->SetExtractionRegion(ExtractionRegion);
		Extractor->SetDirectionCollapseToIdentity();
		TRY_AND_EXIT_ON_ITK_EXCEPTION(Extractor->Update());
		Projections2D.push_back(Extractor->GetOutput());

		TEST_SET_GET_VALUE(2, Projections2D[i]->GetImageDimension());
		TEST_SET_GET_VALUE(500, Projections2D[i]->GetLargestPossibleRegion().GetSize()[0]);
		TEST_SET_GET_VALUE(400, Projections2D[i]->GetLargestPossibleRegion().GetSize()[1]);
	}

	std::cerr << " * Add the fixed image and projection geometry to the registration" << std::endl;
	for (auto i = 0; i < Projections.size(); ++i)
	{
		ObjectRegistration->AddFixedImageAndGeometry(Projections2D[i], Geometries[i]);
		TEST_SET_GET_VALUE(i+1, ObjectRegistration->GetNumberOfFixedImages());
	}

	std::cerr << " * Get the mth fixed image and projection geometry" << std::endl;
	TEST_SET_GET_VALUE(Geometries[1], ObjectRegistration->GetMthProjectionGeometry(1));
	TEST_SET_GET_VALUE(Projections2D[1], ObjectRegistration->GetMthFixedImage(1));

	std::cerr << " * Remove the mth fixed image and projection geometry" << std::endl;
	ObjectRegistration->RemoveMthFixedImage(1);
	TEST_SET_GET_VALUE(2, ObjectRegistration->GetNumberOfFixedImages());

	std::cerr << " * Add the fixed image and change projection geometry" << std::endl;
	ObjectRegistration->AddFixedImageAndGeometry(Projections2D[1], Geometries[1]);
	Geometries[1]->Clear();
	Geometries[1]->AddProjection(500, 900, 90);
	ObjectRegistration->SetMthProjectionGeometry(1, Geometries[1]);
	TEST_SET_GET_VALUE(
		Geometries[1]->GetSourceToDetectorDistances()[0],
		ObjectRegistration->GetMthProjectionGeometry(1)->GetSourceToDetectorDistances()[0]);
	TEST_SET_GET_VALUE(
		Geometries[1]->GetSourceToIsocenterDistances()[0],
		ObjectRegistration->GetMthProjectionGeometry(1)->GetSourceToIsocenterDistances()[0]);
	TEST_SET_GET_VALUE(
		Geometries[1]->GetGantryAngles()[0],
		ObjectRegistration->GetMthProjectionGeometry(1)->GetGantryAngles()[0]);
	Geometries[1]->Clear();
	Geometries[1]->AddProjection(400, 800, 30); // reset geometry object

	std::cerr << " * Try to set projection geometry to non existent fixed image" << std::endl;
	auto failed_geometry = rtk::ThreeDCircularProjectionGeometry::New();
	failed_geometry->AddProjection(700, 1000, 0);
	try
	{
		ObjectRegistration->SetMthProjectionGeometry(Projections2D.size() + 1, failed_geometry);		
	}
	catch (...)
	{
		return EXIT_FAILURE;
	}
	
	std::cerr << " * Remove all fixed images and projection geometries" << std::endl;
	ObjectRegistration->RemoveAllFixedImagesAndProjectionGeometries();
	TEST_SET_GET_VALUE(0, ObjectRegistration->GetNumberOfFixedImages());

	std::cerr << " * Add fixed images and projection geometries" << std::endl;
	for (auto i = 0; i < Projections.size(); ++i)
	{
		ObjectRegistration->AddFixedImageAndGeometry(Projections2D[i], Geometries[i]);
		TEST_SET_GET_VALUE(i + 1, ObjectRegistration->GetNumberOfFixedImages());
	}

	std::cerr << "COMPONENT HANDLING" << std::endl;
	std::cerr << " * DRRENGINE COMPONENT TEST" << std::endl;
	std::cerr << " * * Create and set DRR engine master" << std::endl;
	auto DRREngine = rtk::CudaForwardProjectionImageFilter<Image3DType>::New();
	ObjectRegistration->SetDRREngineMaster(DRREngine);
	TEST_SET_GET_VALUE(DRREngine, ObjectRegistration->GetDRREngineMaster());

	std::cerr << " * * Create and set DRR engine at mth fixed image" << std::endl;
	auto DRREngine2 = rtk::CudaForwardProjectionImageFilter<Image3DType>::New();
	ObjectRegistration->SetMthDRREngine(2, DRREngine2);
	TEST_SET_GET_VALUE(DRREngine2, ObjectRegistration->GetMthDRREngine(2));

	std::cerr << " * * Create and set DRR engine at a too large index" << std::endl;
	auto DRREngine3 = rtk::CudaForwardProjectionImageFilter<Image3DType>::New();
	try
	{
		ObjectRegistration->SetMthDRREngine(Projections2D.size()+1, DRREngine3);
	}
	catch (...)
	{
		return EXIT_FAILURE;
	}

	std::cerr << " * METRIC COMPONENT TEST" << std::endl;
	std::cerr << " * * Create and set a mean squares metric" << std::endl;
	auto MSMetric = itk::MeanSquaresImageToImageMetric<Image2DType, Image2DType>::New();
	ObjectRegistration->SetMetricMaster(MSMetric);
	TEST_SET_GET_VALUE(MSMetric, ObjectRegistration->GetMetricMaster());

	std::cerr << " * * Create and set a mutual information metric" << std::endl;
	auto MIMetric = itk::NormalizedMutualInformationHistogramImageToImageMetric<Image2DType, Image2DType>::New();
	ObjectRegistration->SetMetricMaster(MIMetric);
	TEST_SET_GET_VALUE(MIMetric, ObjectRegistration->GetMetricMaster());

	std::cerr << " * * Get the nth Composite Metric" << std::endl;
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthCompositeMetric(0));
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthCompositeMetric(1));

	std::cerr << " * TRANSFORM COMPONENT TEST" << std::endl;
	std::cerr << " * * Create and set rigid transform" << std::endl;
	auto EulerTransform = itk::Euler3DTransform<double>::New();
	ObjectRegistration->SetTransformMaster(EulerTransform);
	TEST_SET_GET_VALUE(EulerTransform, ObjectRegistration->GetTransformMaster());

	std::cerr << " * * Create and set affine transform" << std::endl;
	auto AffineTransform = itk::AffineTransform<double>::New();
	ObjectRegistration->SetTransformMaster(AffineTransform);
	TEST_SET_GET_VALUE(AffineTransform, ObjectRegistration->GetTransformMaster());

	std::cerr << " * * Create and set deformable transform" << std::endl;
	auto BsplineTransform = itk::BSplineTransform<double>::New();
	ObjectRegistration->SetTransformMaster(BsplineTransform);
	TEST_SET_GET_VALUE(BsplineTransform, ObjectRegistration->GetTransformMaster());

	std::cerr << " * OPTIMIZER COMPONENT TEST" << std::endl;
	std::cerr << " * * Create and set an GradientDescent optimizer" << std::endl;
	auto GradientDescentOpt = itk::GradientDescentOptimizer::New();
	ObjectRegistration->SetOptimizerMaster(GradientDescentOpt);
	TEST_SET_GET_VALUE(GradientDescentOpt, ObjectRegistration->GetOptimizerMaster());

	std::cerr << " * * Create and set an OnePlusOneEvolutionary optimizer" << std::endl;
	auto OnePlusOneEvolOpt = itk::OnePlusOneEvolutionaryOptimizer::New();
	ObjectRegistration->SetOptimizerMaster(OnePlusOneEvolOpt);
	TEST_SET_GET_VALUE(OnePlusOneEvolOpt, ObjectRegistration->GetOptimizerMaster());

	std::cerr << " * * Create and set an Powell optimizer" << std::endl;
	auto PowellOpt = itk::PowellOptimizer::New();
	ObjectRegistration->SetOptimizerMaster(PowellOpt);
	TEST_SET_GET_VALUE(PowellOpt, ObjectRegistration->GetOptimizerMaster());

	std::cerr << "INITIALIZATION TEST" << std::endl;
	std::cerr << " * Check components before initialization" << std::endl;
	std::cerr << " * * Check engines" << std::endl;
	auto m_min = 0;
	auto m_max = ObjectRegistration->GetNumberOfFixedImages();
	for (auto m = m_min; m < m_max +1; ++m)
	{
		TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetMthDRREngine(m));
	}

	std::cerr << " * * Check metrics" << std::endl;
	auto n_min = 0;
	auto n_max = ObjectRegistration->GetNumberOfObjects();
	for (auto n = n_min; n < n_max; ++n)
	{
		if (ObjectRegistration->GetNthCompositeMetric(n) == nullptr)
			return EXIT_FAILURE;
	}
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthCompositeMetric(n_max + 1));

	std::cerr << " * * Check initial transforms" << std::endl;
	for (auto n = n_min; n < n_max; ++n)
	{
		if (ObjectRegistration->GetNthInitialTransform(n) == nullptr)
			return EXIT_FAILURE;
	}
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthInitialTransform(n_max + 1));

	std::cerr << " * * Check internal transforms" << std::endl;
	for (auto n = n_min; n < n_max; ++n)
	{
		if (ObjectRegistration->GetNthTransform(n) == nullptr)
			return EXIT_FAILURE;
	}
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthTransform(n_max + 1));

	std::cerr << " * * Check optimizer" << std::endl;
	for (auto n = n_min; n < n_max; ++n)
	{
		if (ObjectRegistration->GetNthOptimizer(n) == nullptr)
			return EXIT_FAILURE;
	}
	TEST_SET_GET_NULL_VALUE(ObjectRegistration->GetNthOptimizer(n_max + 1));

	std::cerr << " * Check components after initialization" << std::endl;

	std::cerr << "OUTPUT CHECK" << std::endl;
	std::cerr << " * Number of outputs" << std::endl;
	//TEST_SET_GET_VALUE(3, ObjectRegistration->GetNumberOfIndexedOutputs());

	

	

	
	return EXIT_SUCCESS;
}


/** Helper functions */
void
CreateObjectImage(Image3DPointer Image)
{
	std::cerr << " * * CreateObjectImage..." << std::endl;
	Image3DType::IndexType idx;
	idx.Fill(0);
	Image3DType::SizeType sz;
	sz.Fill(200);

	Image3DType::RegionType reg(idx, sz);
	Image->SetRegions(reg);
	Image->Allocate();
	Image->FillBuffer(0);

	for (unsigned int r = 20; r < 80; r++)
	{
		for (unsigned int c = 30; c < 100; c++)
		{
			for (unsigned int d = 40; d < 80; d++)
			{
				Image3DType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 1);
			}
		}
	}

	// Set pixels in a different square to a different value
	for (unsigned int r = 100; r < 130; r++)
	{
		for (unsigned int c = 115; c < 160; c++)
		{
			for (unsigned int d = 115; d < 170; d++)
			{
				Image3DType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 2);
			}
		}
	}

	int center[] = { 60,70,150 };
	auto radius = 20;
	for (unsigned int r = 40; r < 80; r++)
	{
		for (unsigned int c = 50; c < 90; c++)
		{
			for (unsigned int d = 130; d < 170; d++)
			{
				auto r_c = (r - center[0]) * (r - center[0]);
				auto c_c = (c - center[1]) * (c - center[1]);
				auto d_c = (d - center[2]) * (d - center[2]);
				if (sqrt(r_c + c_c + d_c) < radius)
				{
					Image3DType::IndexType pixelIndex;
					pixelIndex[0] = r;
					pixelIndex[1] = c;
					pixelIndex[2] = d;

					Image->SetPixel(pixelIndex, 3);
				}
			}
		}
	}
}

TransformType::MatrixType
CreateRotationTransform(double x, double y, double z)
{
	std::cerr << " * * CreateRotationTransform..." << std::endl;
	auto Transform = TransformType::New();
	Transform->SetRotation(x, y, z);
	return Transform->GetMatrix();
}

void
CreateProjectorImage(Image3DPointer pi, int DetectorR, int DetectorC, double SpacingR, double SpacingC)
{
	std::cerr << " * * CreateProjectorImage..." << std::endl;
	Image3DType::IndexType DetectorIndex;
	DetectorIndex.Fill(0);
	Image3DType::SizeType DetectorSize;
	DetectorSize[0] = DetectorR;
	DetectorSize[1] = DetectorC;
	DetectorSize[2] = 1;
	Image3DType::RegionType DetectorRegion(DetectorIndex, DetectorSize);
	pi->SetRegions(DetectorRegion);
	pi->Allocate();
	Image3DType::SpacingType spacing;
	spacing[0] = SpacingR;
	spacing[1] = SpacingC;
	spacing[2] = 1;
	pi->SetSpacing(spacing);
}

void
WriteImage(Image3DPointer Image, std::string FileName)
{
	std::cerr << " * * WriteImage..." << std::endl;
	FileName.append(".nrrd");
	auto Writer = ImageWriterType::New();
	Writer->SetFileName(FileName);
	Writer->SetInput(Image);

	TRY_AND_EXIT_ON_ITK_EXCEPTION(Writer->Update());
}

