#include "itkCompositeImageToImageMetric.h"

// STL
#include <cstdlib>
// RTK
#include "itkCudaImage.h"
// ITK
#include "itkMeanSquaresImageToImageMetric.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkTranslationTransform.h"
#include "itkTestingMacros.h"

int itkCompositeImageToImageMetricTest(int , char*[])
{
	auto TestResult = EXIT_SUCCESS;

	/** Standard definitions */
	const unsigned int Dim3 = 3;
	const unsigned int Dim2 = 2;
	using Pixeltype = float;
	using Image3DType = itk::CudaImage<Pixeltype, Dim3>;
	using Image3DPointer = Image3DType::Pointer;
	using Image2DType = itk::CudaImage<Pixeltype, Dim2>;
	using Image2DPointer = Image2DType::Pointer;

	/** Create input metric*/
	using ImageMetricType = itk::MeanSquaresImageToImageMetric<Image2DType, Image2DType>;
	using ImageMetricPointer = ImageMetricType::Pointer;
	using BaseMetricType = itk::ImageToImageMetric<Image2DType, Image2DType>;
	using BaseMetricPointer = typename BaseMetricType::Pointer;
	BaseMetricPointer ImageMetric = nullptr;

	/** Create CompositeImageToImageMetric */
	auto CompositeImageMetric = itk::CompositeImageToImageMetric<Image2DType, Image2DType>::New();
	TEST_SET_GET_VALUE(1, CompositeImageMetric->GetNumberOfSubmetrics());

	/** Set metric before initializing */
	auto MSMetric = ImageMetricType::New();
	ImageMetric = MSMetric;

	CompositeImageMetric->SetSubmetric(ImageMetric);
	TEST_SET_GET_VALUE(ImageMetric, CompositeImageMetric->GetNthSubmetric(0));
	TEST_SET_GET_VALUE(1, CompositeImageMetric->GetNumberOfSubmetrics());

	/** Set number of submetrics */
	auto numberofmetrics = 5;
	CompositeImageMetric->SetNumberOfSubmetrics(numberofmetrics);
	TEST_SET_GET_VALUE(numberofmetrics, CompositeImageMetric->GetNumberOfSubmetrics());

	/** All new metrics are nullptr (only the vector was resized) */
	for (auto i = 1; i < numberofmetrics; ++i)
	{
		TEST_SET_GET_NULL_VALUE(CompositeImageMetric->GetNthSubmetric(i));
	}

	/** Initialize the submetrics and test the instance types */
	try
	{
		CompositeImageMetric->Initialize();
	}
	catch (itk::ExceptionObject &EO)
	{
		EO.Print(std::cerr);
	}

	std::string MetricTypeName = ImageMetric->GetNameOfClass();
	for (auto i = 0; i < numberofmetrics; ++i)
	{
		TEST_SET_GET_VALUE(MetricTypeName, CompositeImageMetric->GetNthSubmetric(i)->GetNameOfClass());
	}

	/** Set individual weights to the submetrics*/
	double weights[] = { 1,2,3,4,5 };
	for (auto i = 0; i < numberofmetrics; ++i)
	{
		CompositeImageMetric->SetNthSubmetricWeight(i,weights[i]);
		TEST_SET_GET_VALUE(weights[i], CompositeImageMetric->GetNthSubmetricWeight(i));
	}

	/** Input tests */


	
	
	/** Remove all submetrics*/
	CompositeImageMetric->RemoveAllSubmetrics();
	TEST_SET_GET_VALUE(0, CompositeImageMetric->GetNumberOfSubmetrics());
	
	return TestResult;
}

